package constants;

/**
 * Created by Chokkar on 22/12/2016.
 */

public interface IConstants {

    public static final String TOKEN_VALUE = "token_value";
    public static final String AUTH_USER_ID = "auth_id";
    public static final String USERNAME = "username";
}
